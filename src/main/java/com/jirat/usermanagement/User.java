/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.usermanagement;

import java.io.Serializable;

/**
 *
 * @author ACER
 * User Model
 */
public class User implements Serializable{
    private String userName;
    private String password;

    public User(String userNme, String password) {
        this.userName = userNme;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userNme) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "userName=" + userName + ", password=" + password;
    }
    
}
